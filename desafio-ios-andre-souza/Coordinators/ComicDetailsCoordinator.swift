//
//  ComicCoordinator.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 08/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import Foundation
import RxSwift

class ComicDetailsCoordinator: BaseCoordinator {
    
    var viewController: ComicDetailsViewController!
    var characterSelected: CharacterModel!
    var disposeBag = DisposeBag()
    
    init(character: CharacterModel){
        self.characterSelected = character
    }
    
    override func start() {
        
        self.navigationController.navigationBar.isHidden = false
        viewController = ComicDetailsViewController.instantiate()
        
        let viewModel = ComicDetailsViewModel(dataService: DataService(), self.characterSelected)
        viewController.viewModel = viewModel
        
        viewModel.goBackDidTap
        .subscribe(onNext: { _ in
            self.navigationController.popViewController(animated: true)
        }).disposed(by: self.disposeBag)
        
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
}
