//
//  CharacterDetailsCoordinator.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 08/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import Foundation
import RxSwift

class CharacterDetailsCoordinator: BaseCoordinator {
    
    var viewController: CharacterDetailsViewController!
    var characterSelected: CharacterModel!
    var disposeBag = DisposeBag()
    
    init(character: CharacterModel){
        self.characterSelected = character
    }
    
    override func start() {
        
        self.navigationController.navigationBar.isHidden = false
        viewController = CharacterDetailsViewController.instantiate()
        
        let viewModel = CharacterDetailsViewModel(self.characterSelected)
        viewController.viewModel = viewModel
        
        viewModel.buttonComicDidTap
            .subscribe(onNext: { character in
                (self.parentCoordinator as? CoordinatorPageNavigation)?.goToComicDetails(character: character)
            }).disposed(by: self.disposeBag)
        
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
}
