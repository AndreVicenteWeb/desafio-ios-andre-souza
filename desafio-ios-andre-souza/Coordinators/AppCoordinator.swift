//
//  AppCoordinator.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 07/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import UIKit

class AppCoordinator: BaseCoordinator {

    init(navigationController: UINavigationController) {
        super.init()
        self.navigationController = navigationController
    }

    override func start() {
        
        goToCharactersPage()
    }
}

protocol CoordinatorPageNavigation {
    func goToCharactersPage()
    func goToDetailsFrom(character: CharacterModel)
    func goToComicDetails(character: CharacterModel)
}

extension AppCoordinator: CoordinatorPageNavigation {

    func goToCharactersPage(){
        let coordinator = CharactersCoordinator()
        coordinator.navigationController = self.navigationController
        self.start(coordinator: coordinator)
    }
    
    func goToDetailsFrom(character: CharacterModel) {
        let coordinator = CharacterDetailsCoordinator(character: character)
        coordinator.navigationController = self.navigationController
        self.start(coordinator: coordinator)
    }
    
    func goToComicDetails(character: CharacterModel) {
        let coordinator = ComicDetailsCoordinator(character: character)
        coordinator.navigationController = self.navigationController
        self.start(coordinator: coordinator)
    }
}
