//
//  MainCoordinator.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 07/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import Foundation
import RxSwift

class CharactersCoordinator: BaseCoordinator {
    
    var viewController: CharactersTableViewController!
    var disposeBag = DisposeBag()
    
    override func start() {
        
        self.navigationController.navigationBar.isHidden = false
        
        viewController = CharactersTableViewController.instantiate()
        
        let viewModel = CharactersViewModel(dataService: DataService())
        viewController.viewModel = viewModel
        
        viewModel.characterDidTap
            .subscribe(onNext: { character in
                (self.parentCoordinator as? CoordinatorPageNavigation)?.goToDetailsFrom(character: character)
        }).disposed(by: self.disposeBag)
        
        self.navigationController.viewControllers = [viewController]
    }
    
}
