//
//  DataService.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 08/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

struct ApiError: Error {
    let message: String

    init(_ message: String) {
        self.message = message
    }

    public var localizedDescription: String {
        return message
    }
}

struct DataService {
    
    static let shared = DataService()
    
    private let publicKey = "be804f807f3f9deb8dd0412dd595ba2c"
    private let privateKey = "b76ff34fd3308435108c57e63d5e15d738c0f346"
    private var charactersUrl = "http://gateway.marvel.com/v1/public/characters"
    private var comicsUrl = "http://gateway.marvel.com/v1/public/characters/@%/comics"
    
    func getCharactes(currentIndex: Int, totalPagination: Int) -> Observable<[CharacterModel]>{
        return Observable.create { (observer) -> Disposable in
            
            let url = "\(self.charactersUrl)\(self.getAuthParams())&limit=\(totalPagination)&offset=\(currentIndex)"
            
            AF.request(url)
                .responseJSON { (response) in
                switch response.result {
                case .success:
                    
                    guard let data = response.data else {
                        return
                    }
                    
                    do {
                        let dataModel = try JSONDecoder().decode(CharacterDataWrapper.self, from: data)
                        if(dataModel.code == "200"){
                            observer.onNext(dataModel.data!.results)
                        }else{
                            observer.onError(ApiError(dataModel.message!))
                        }
                    } catch {
                        observer.onError(error)
                    }
                case .failure(let error):
                    //print("erro: \(response) \(error.errorDescription) \(error.localizedDescription)")
                    observer.onError(error)
                }
            }
            
            return Disposables.create()
        }
    }
    
    func getComicsBy(id: Int) -> Observable<[ComicModel]>{
        return Observable.create { (observer) -> Disposable in
            
            var url = "\(self.comicsUrl)\(self.getAuthParams())"
            url = url.replacingOccurrences(of: "@%", with: "\(id)")
            
            AF.request(url)
                .responseJSON { (response) in
                switch response.result {
                case .success:
                    
                    guard let data = response.data else {
                        return
                    }
                    
                    do {
                        let dataModel = try JSONDecoder().decode(ComicDataWrapper.self, from: data)
                        if(dataModel.code == "200"){
                            observer.onNext(dataModel.data!.results)
                        }else{
                            observer.onError(ApiError(dataModel.message!))
                        }
                    } catch {
                        //print("ERROR: \(error)")
                        observer.onError(error)
                    }
                case .failure(let error):
                    print("erro: \(response)")
                    observer.onError(error)
                }
            }
            
            return Disposables.create()
        }
    }
    
    private func getAuthParams() -> String{
        
        let timestamp = Utils.generateCurrentTimeStamp()
        
        return "?ts=\(timestamp)&apikey=\(self.publicKey)&hash=\(self.getHash(timestamp))"
    }
    
    private func getHeaders() -> HTTPHeaders{
        
        let timestamp = Utils.generateCurrentTimeStamp()
        
        let headers: HTTPHeaders = [
            "ts": timestamp,
            "apikey": self.publicKey,
            "hash": self.getHash(timestamp)
        ]
        
        return headers
    }
    
    private func getHash(_ timestamp: String) -> String{
        return Utils.MD5(timestamp+self.privateKey+self.publicKey) ?? ""
    }
}
