//
//  ComicDetailsViewModel.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 08/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import Foundation
import RxSwift

class ComicDetailsViewModel {
    
    var comicSelected = BehaviorSubject<ComicModel?>(value: nil)
    let showLoading = BehaviorSubject<Bool>(value: false)
    let showError = BehaviorSubject<Error?>(value: nil)
    var isLoading = false
    let goBackDidTap = PublishSubject<Any>()
    private let disposeBag = DisposeBag()
    
    init(dataService: DataService, _ chatacter: CharacterModel) {
     
        self.showLoading.onNext(true)
        
        dataService.getComicsBy(id: chatacter.id)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] comics in
                
                self?.showLoading.onNext(false)
                if(comics.count > 0){
                    self?.prepareToShow(comics)
                }else{
                    self?.showError.onNext(ApiError("Nenhuma revista encontrada"))
                }
                
            }, onError: { [weak self] error in
                self?.showLoading.onNext(false)
                self?.showError.onNext(error)
            }).disposed(by: self.disposeBag)
    }
    
    private func prepareToShow(_ comics: [ComicModel]){
        
        let mostValuedComic : ComicModel? = comics.max(by: {$0.getMostValuePrice() < $1.getMostValuePrice()})
        if let comic = mostValuedComic {
            self.comicSelected.onNext(comic)
        }
    }
    
    func goBack(){
        self.goBackDidTap.onNext(())
    }
}
