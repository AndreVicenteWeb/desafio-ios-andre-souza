//
//  CharacterDetailsViewModel.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 08/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import Foundation
import RxSwift

class CharacterDetailsViewModel {
    
    var charatecterSelected = BehaviorSubject<CharacterModel?>(value: nil)
    let buttonComicDidTap = PublishSubject<CharacterModel>()
    
    init(_ chatacter: CharacterModel) {
        self.charatecterSelected.onNext(chatacter)
    }
    
    func comicDidTap(){
        guard let character = try? self.charatecterSelected.value() else { return }
        self.buttonComicDidTap.onNext(character)
    }
}
