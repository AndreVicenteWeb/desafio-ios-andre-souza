//
//  MainViewModel.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 07/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import Foundation
import RxSwift

class CharactersViewModel {
    
    var charactersArray = PublishSubject<[CharacterModel]>()
    var characterDidTap = PublishSubject<CharacterModel>()
    var isLoading = false
    let showLoading = BehaviorSubject<Bool>(value: false)
    let showError = BehaviorSubject<Error?>(value: nil)
    var currentIndex = 0
    var totalPagination = 20
    var dataService: DataService!
    var data = [CharacterModel]()
    
    private let disposeBag = DisposeBag()
    
    init(dataService: DataService) {
    
        self.dataService = dataService
        self.requestCharacters()
    }
    
    func requestCharacters(){
        
        if(self.isLoading){ return }
        
        self.showLoading.onNext(true)
        
        dataService.getCharactes(currentIndex: self.currentIndex, totalPagination: self.totalPagination)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] character in
                
                guard let self = self else { return }
                self.currentIndex += self.totalPagination
                self.showLoading.onNext(false)
                self.data += character
                self.charactersArray.onNext(self.data)
            }, onError: { [weak self] error in
                self?.showLoading.onNext(false)
                print(error)
                self?.showError.onNext(error)
            }).disposed(by: self.disposeBag)
    }
    
    func getDetailsFrom(character: CharacterModel){
        self.characterDidTap.onNext(character)
    }
}
