//
//  CharactersTableViewCell.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 07/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher

class CharactersTableViewCell: UITableViewCell {

    static var identifier = "characterTableViewCell"
    
    // MARK: Outlets
    @IBOutlet weak var imageViewThumb: UIImageView!
    @IBOutlet weak var labelCharacterName: UILabel!
    
    var disposeBag = DisposeBag()


    override func prepareForReuse() {
        super.prepareForReuse()

        disposeBag = DisposeBag()
    }
    
    
    var character: CharacterModel? {
        
        didSet{
            guard let character = character else { return }
            labelCharacterName.text = character.name
            imageViewThumb.image = nil
            
            let url = URL(string: character.fullImagePath(fromSize: .standard_small))
            imageViewThumb.kf.setImage(with: url)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
