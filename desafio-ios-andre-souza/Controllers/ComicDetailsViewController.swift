//
//  ComicDetailsViewController.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 08/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import UIKit
import RxSwift

class ComicDetailsViewController: UIViewController, Storyboarded {

    @IBOutlet weak var imageViewThumb: UIImageView!
    @IBOutlet weak var labelComicName: UILabel!
    @IBOutlet weak var labelComicDescription: UILabel!
    @IBOutlet weak var labelComicPrice: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var viewContent: UIView!
    
    var viewModel: ComicDetailsViewModel!
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bind()
    }
    
    private func bind(){
        
        self.viewModel.showLoading
        .asObserver()
        .subscribe(onNext: { [weak self] showLoading in
            guard let self = self else { return }
            
            if(showLoading){
                self.viewContent.isHidden = true
                self.activityIndicator.isHidden = false
                self.activityIndicator.startAnimating()
            }else{
                self.viewContent.isHidden = false
                self.activityIndicator.isHidden = true
                self.activityIndicator.startAnimating()
            }
        }).disposed(by: self.disposeBag)
    
        self.viewModel.showError
        .asObserver()
        .subscribe(onNext: { [weak self] error in
            guard let self = self else { return }
            guard let error = error else { return }
            var message = error.localizedDescription
            if let erro = error as? ApiError{
                message = erro.message
            }
            
            self.viewContent.isHidden = true
            
            self.dismiss(animated: true, completion: nil)
            Utils.showAlert(title: "Erro", text: message, view: self) { (UIAlertAction) in
                self.viewModel.goBack()
            }

        }).disposed(by: self.disposeBag)
        
        self.viewModel.comicSelected
        .asObserver()
        .subscribe(onNext: { [weak self] comic in
            
            guard let comic = comic else { return }
            
            self?.labelComicName.text = comic.title
            self?.labelComicDescription.text = comic.description
            self?.labelComicPrice.text = "$ \(comic.getMostValuePrice())"
            
            let url = URL(string: comic.fullImagePath(fromSize: .portrait_uncanny))
            self?.imageViewThumb.kf.setImage(with: url)
            
        }).disposed(by: self.disposeBag)
    }
}
