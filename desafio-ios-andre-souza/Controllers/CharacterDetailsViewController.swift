//
//  CharacterDetailsViewController.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 08/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher

class CharacterDetailsViewController: UIViewController, Storyboarded {

    @IBOutlet weak var imageViewThumb: UIImageView!
    @IBOutlet weak var labelCharacterName: UILabel!
    @IBOutlet weak var labelCharacterDescription: UILabel!
    @IBOutlet weak var buttonComics: UIButton!
    
    var viewModel: CharacterDetailsViewModel!
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bind()
    }
    
    private func bind(){
        
        self.viewModel.charatecterSelected
        .asObserver()
        .subscribe(onNext: { [weak self] character in
            
            guard let character = character else { return }
            
            self?.labelCharacterName.text = character.name
            self?.labelCharacterDescription.text = character.description
            
            let url = URL(string: character.fullImagePath(fromSize: .landscape_large))
            self?.imageViewThumb.kf.setImage(with: url)
            
        }).disposed(by: self.disposeBag)
        
        self.buttonComics.rx.tap
            .bind{ self.viewModel.comicDidTap() }
            .disposed(by: self.disposeBag)
    }
}
