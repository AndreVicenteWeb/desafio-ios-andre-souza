//
//  MainTableViewController.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 07/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CharactersTableViewController: UITableViewController, Storyboarded {
    
    var viewModel: CharactersViewModel!
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureTable()
        bind()
    }
    
    private func configureTable(){
        
        tableView.dataSource = nil
        tableView.delegate = nil
        tableView.rowHeight = 70.0
        tableView.tableFooterView = getSpinner()
    }
    
    private func getSpinner() -> UIActivityIndicatorView{
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
        return spinner
    }
    
    private func bind(){
        
        self.viewModel.charactersArray
            .asObserver()
            .bind(to: tableView.rx.items(cellIdentifier: CharactersTableViewCell.identifier, cellType: CharactersTableViewCell.self)){ row, character, cell in
                
                cell.character = character
                
        }.disposed(by: self.disposeBag)
        
        self.viewModel.showLoading
            .asObserver()
            .subscribe(onNext: { [weak self] showLoading in
                guard let self = self else { return }
                self.viewModel.isLoading = showLoading
                self.tableView.tableFooterView?.isHidden = !showLoading
            }).disposed(by: self.disposeBag)
        
        self.viewModel.showError
            .asObserver()
            .subscribe(onNext: { [weak self] error in
                guard let self = self else { return }
                guard let error = error else { return }
                var message = error.localizedDescription
                if let erro = error as? ApiError{
                    message = erro.message
                }
                Utils.showAlert(title: "Erro", text: message, view: self)
            }).disposed(by: self.disposeBag)
        
        self.tableView.rx.modelSelected(CharacterModel.self).subscribe(onNext: { character in
            self.viewModel.getDetailsFrom(character: character)
        }).disposed(by: self.disposeBag)
        
        self.tableView.rx.contentOffset.subscribe({ offset in
            if self.isNearTheBottomEdge(contentOffset: offset.element!, self.tableView) {
                self.viewModel.requestCharacters()
            }
            
        }).disposed(by: self.disposeBag)
    }
    
    let startLoadingOffset: CGFloat = 20.0

    func isNearTheBottomEdge(contentOffset: CGPoint, _ tableView: UITableView) -> Bool {
        return contentOffset.y + tableView.frame.size.height + startLoadingOffset > tableView.contentSize.height
    }
}
