//
//  Utils.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 08/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import UIKit
import CommonCrypto

struct Utils {
    
    static func MD5(_ string: String) -> String? {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        var digest = [UInt8](repeating: 0, count: length)

        if let d = string.data(using: String.Encoding.utf8) {
            _ = d.withUnsafeBytes { (body: UnsafePointer<UInt8>) in
                CC_MD5(body, CC_LONG(d.count), &digest)
            }
        }

        return (0..<length).reduce("") {
            $0 + String(format: "%02x", digest[$1])
        }
    }
    
    static func generateCurrentTimeStamp () -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy_MM_dd_hh_mm_ss"
        return (formatter.string(from: Date()) as NSString) as String
    }
    
    static func showAlert(title: String, text: String, view: UIViewController, completition: ((UIAlertAction) -> Void)? = nil) {
        let okAlertAction = UIAlertAction(title: "Ok", style: .default, handler: completition)
        let alertViewController = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alertViewController.addAction(okAlertAction)
        view.present(alertViewController, animated: true, completion: nil)
    }
}
