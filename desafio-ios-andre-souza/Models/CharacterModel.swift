//
//  CharacterModel.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 07/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import Foundation

enum ImageSize{
    case portrait_small
    case portrait_medium
    case portrait_xlarge
    case portrait_fantastic
    case portrait_incredible
    case portrait_uncanny
    case standard_small
    case standard_medium
    case landscape_small
    case landscape_medium
    case landscape_large
}

struct CharacterDataWrapper: Codable {
    var data: CharacterDataContainer?
    var code: String?
    var message: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.data = try container.decodeIfPresent(CharacterDataContainer.self, forKey: .data)
        self.message = try container.decodeIfPresent(String.self, forKey: .message)
        
        if let code = try? container.decodeIfPresent(String.self, forKey: .code){
            self.code = code
        }else if let code = try? container.decodeIfPresent(Int.self, forKey: .code){
            self.code = "\(code)"
        }
    }
}

struct CharacterDataContainer: Codable {
    var results: [CharacterModel]
}

struct CharacterModel: Codable {
    var id: Int
    var name: String
    var description: String
    var thumbnail: ThumbModel
    
    func fullImagePath(fromSize: ImageSize) -> String {
        return "\(self.thumbnail.path)/\(fromSize).\(self.thumbnail.extension)"
    }
}

struct ThumbModel: Codable {
    var path: String
    var `extension`: String
}
