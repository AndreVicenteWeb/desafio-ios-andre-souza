//
//  ComicModel.swift
//  desafio-ios-andre-souza
//
//  Created by André Vicente on 08/02/20.
//  Copyright © 2020 André Vicente. All rights reserved.
//

import Foundation

struct ComicDataWrapper: Codable {
    var data: ComicDataContainer?
    var code: String?
    var message: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.data = try container.decodeIfPresent(ComicDataContainer.self, forKey: .data)
        self.message = try container.decodeIfPresent(String.self, forKey: .message)
        
        if let code = try? container.decodeIfPresent(String.self, forKey: .code){
            self.code = code
        }else if let code = try? container.decodeIfPresent(Int.self, forKey: .code){
            self.code = "\(code)"
        }
    }
}

struct ComicDataContainer: Codable {
    var results: [ComicModel]
}

struct ComicModel : Codable {
    
    var id: Int
    var title: String
    var description: String?
    var thumbnail: ThumbModel
    var prices: [Price]
    
    func fullImagePath(fromSize: ImageSize) -> String {
        return "\(self.thumbnail.path)/\(fromSize).\(self.thumbnail.extension)"
    }
    
    func getMostValuePrice() -> Float{
        return self.prices.map { $0.price }.max() ?? 0.00
    }
}

struct Price : Codable {
    var type: String
    var price: Float
}
